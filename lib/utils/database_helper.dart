import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:flutter_note_keeper/model/note.dart';
import 'dart:async';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static DatabaseHelper _databaseHelper; //Singleton Database Helper Object
  static Database _database; //Singleton Database Object

  String noteTable = "note_table";
  String colId = "id";
  String colTitle = "title";
  String colDescription = "description";
  String colDate = "date";
  String colPriority = "priority";

  DatabaseHelper._createInstance(); // Named constructor to create instance DatabaseHelper

  factory DatabaseHelper() {
    if (_databaseHelper == null) {
      _databaseHelper = DatabaseHelper
          ._createInstance(); // This is executed only once , singleton object
    }
    return _databaseHelper;
  }

  void _createDB(Database database, int version) async {
    await database.execute('CREATE TABLE $noteTable('
        '$colId INTEGER PRIMARY KEY AUTOINCREMENT,'
        '$colTitle TEXT, '
        '$colDescription TEXT, '
        '$colDate TEXT, '
        '$colPriority INTEGER)');
  }

  Future<Database> initializeDatabase() async {
    //Get the directory path for both android and ios to store database
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + 'notes.db';

    //Open/create the database at a given path
    var notesDatabase =
        await openDatabase(path, version: 1, onCreate: _createDB);
    return notesDatabase;
  }

  Future<Database> get database async {
    // Getter for _database
    if (_database == null) {
      _database = await initializeDatabase();
    }
    return _database;
  }

  //FETCH OPERATION : Get All Note Object Data From Database
  Future<List<Map<String, dynamic>>> getNoteMapList() async {
    Database database = await this.database;
    var results = await database
        .rawQuery("SELECT * FROM $noteTable ORDER BY $colPriority ASC");
    var result = await database.query(noteTable, orderBy: '$colPriority ASC');

    return results;
  }

  //INSERT OPERATION : Insert a new note to database
  Future<int> insertNoteToDataBase(Note note) async {
    Database database = await this.database;
    var result = await database.insert(noteTable, note.toMap());
    return result;
  }

  //UPDATE OPERATION : Update a note object and save it to database
  Future<int> updateNoteData(Note note) async {
    var database = await this.database;
    var result = await database.update(noteTable, note.toMap(),
        where: '$colId = ?', whereArgs: [note.id]);
    return result;
  }

  //DELETE OPERATION : Delete a note object and save it to database
  Future<int> deleteNoteData(int id) async {
    var database = await this.database;
    var result =
        await database.rawDelete('DELETE FROM $noteTable WHERE $colId = $id');
    return result;
  }

  //Get number of note objects in database
  Future<int> getCount() async {
    Database database = await this.database;
    List<Map<String, dynamic>> count =
        await database.rawQuery('SELECT COUNT(*) FROM $noteTable');
    int result = Sqflite.firstIntValue(count);
    return result;
  }

  Future<List<Note>> getNoteList() async {
    var noteMapList = await getNoteMapList();
    int count = noteMapList.length;
    List<Note> noteList = List<Note>();
    for (int i = 0; i < count; i++) {
      noteList.add(Note.fromMapObject(noteMapList[i]));
    }
    return noteList;
  }
}
