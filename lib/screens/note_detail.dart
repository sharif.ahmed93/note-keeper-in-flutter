import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter_note_keeper/model/note.dart';
import 'package:flutter_note_keeper/utils/database_helper.dart';
import 'package:intl/intl.dart';

class NoteDetail extends StatefulWidget {
  final String appbarTitle;
  final Note note;

  NoteDetail(this.note, this.appbarTitle);

  @override
  State<StatefulWidget> createState() {
    return NoteDetailState(this.note, this.appbarTitle);
  }
}

class NoteDetailState extends State<NoteDetail> {
  String appbarTitle;
  Note note;
  static var _priorities = ["High", "Low"];
  var _selectedItem = '';
  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  DatabaseHelper databaseHelper = DatabaseHelper();

  NoteDetailState(this.note, this.appbarTitle);

  @override
  void initState() {
    super.initState();
    _selectedItem = _priorities[0];
  }

  @override
  Widget build(BuildContext context) {
    titleController.text = note.title;
    descriptionController.text = note.description;
    TextStyle textStyle = Theme.of(context).textTheme.title;
    return WillPopScope(
      // ignore: missing_return
      onWillPop: () {
        moveToLastScreen();
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(appbarTitle),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              moveToLastScreen();
            },
          ),
        ),
        body: Padding(
          padding: EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0),
          child: ListView(
            children: <Widget>[
              ListTile(
                title: DropdownButton<String>(
                  items: _priorities.map((String item) {
                    return DropdownMenuItem<String>(
                      child: Text(item),
                      value: item,
                    );
                  }).toList(),
                  style: textStyle,
                  value: getPriorityAsString(note.priority),
                  onChanged: (String newItem) {
                    onDropDownMenuItemSelected(newItem);
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                child: TextField(
                  controller: titleController,
                  style: textStyle,
                  onChanged: (String userTitleInput) {
                    debugPrint("$userTitleInput");
                    updateTitle();
                  },
                  decoration: InputDecoration(
                    labelText: "Title",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                child: TextField(
                  style: textStyle,
                  controller: descriptionController,
                  onChanged: (String userDescriptionInput) {
                    debugPrint(userDescriptionInput);
                    updateDescription();
                  },
                  decoration: InputDecoration(
                    labelText: "Description",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: RaisedButton(
                        child: Text(
                          "Save",
                          textScaleFactor: 1.5,
                        ),
                        color: Theme.of(context).primaryColorDark,
                        textColor: Theme.of(context).primaryColorLight,
                        onPressed: () {
                          setState(() {
                            _saveNote();
                          });
                        },
                      ),
                    ),
                    Container(
                      width: 10.0,
                    ),
                    Expanded(
                      child: RaisedButton(
                        child: Text(
                          "Delete",
                          textScaleFactor: 1.5,
                        ),
                        color: Theme.of(context).primaryColorDark,
                        textColor: Theme.of(context).primaryColorLight,
                        onPressed: () {
                          setState(() {
                            _delete();
                          });
                        },
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void onDropDownMenuItemSelected(String newItem) {
    setState(() {
      getPriorityAsInt(newItem);
    });
  }

  void moveToLastScreen() {
    Navigator.pop(context,true);
  }

  void getPriorityAsInt(String priority) {
    switch (priority) {
      case 'High':
        note.priority = 1;
        break;
      case 'Low':
        note.priority = 2;
        break;
    }
  }

  String getPriorityAsString(int value) {
    String priority;
    switch (value) {
      case 1:
        priority = _priorities[0];
        break;
      case 2:
        priority = _priorities[1];
        break;
    }
    return priority;
  }

  void updateTitle() {
    note.title = titleController.text;
  }

  void updateDescription() {
    note.description = descriptionController.text;
  }

  void _saveNote() async {
    moveToLastScreen();
    int result;
    note.date = DateFormat.yMMMd().format(DateTime.now());
    if (note.id != null) {
      result = await databaseHelper.updateNoteData(note);
    } else {
      result = await databaseHelper.insertNoteToDataBase(note);
    }

    if (result != 0) {
      _showAlertDialog('Status', 'Note Saved Successfully!');
    } else {
      _showAlertDialog('Status', 'Problem Saving Note!');
    }
  }

  void _delete() async {
    moveToLastScreen();
    if (note.id == null) {
      _showAlertDialog('Status', 'No Note Was Deleted!');
      return;
    }
    int result = await databaseHelper.deleteNoteData(note.id);
    if (result != 0) {
      _showAlertDialog('Status', 'Note Deleted Successfully!');
    } else {
      _showAlertDialog('Status', 'Error Occured While Deleting Note!');
    }
  }

  void _showAlertDialog(String status, String message) {
    AlertDialog alertDialog = AlertDialog(
      title: Text(status),
      content: Text(message),
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }
}
